/**
 * 
 */
 
 
var systemTime = new Date().getTime();

function runTime() {
    systemTime += 1000;
    showTime();
}

function showTime() {
    var now = new Date(systemTime);
    var h = "0" + now.getHours();
    var m = "0" + now.getMinutes();
     h = h.substring(h.length - 2, h.length + 1);
     m = m.substring(m.length - 2, m.length + 1);
    var today = h + ":" + m;
    document.getElementById("time").innerHTML = today
    document.getElementById("week").innerHTML = (now.getMonth() + 1) + "/" + now.getDate() + " " + getDay(now.getDay())
    setTimeout("runTime()", 1000);
}
function getDay(day) {
    var days = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
    return days[day];
}

window.runTime = runTime;
window.showTime = showTime;